<?php

include_once('models/Persona.php');
include_once('models/serializarArchivo.php');

class personaController {
    
    public static function procesar()
    {
        $action = ""; 
        if(isset($_REQUEST['action']))
            $action = $_REQUEST['action'];
       
        /** Si se pulsó el botón agregar */
        if($action == "agregar") self::agregar();
        else if($action == "insertar") self::insertar();
        else if($action == "editar") self::editar();
        else if($action == "actualizar") self::actualizar();
        else if($action == "borrar") self::borrar();
        else self::listar();
    }

    public static function agregar()
    {
        $personaActual = new Persona();
        $eventoFormulario = "insertar";
        include_once("views/persona/agregarOeditar.php");
    }

    public static function insertar()
    {
        $listaPersonas = serializarArchivo::deserializar();
        $nuevoPersona = new Persona((int)$_POST['id'], $_POST['nombre'], $_POST['edad'], $_POST['sexo']);      
        array_push($listaPersonas, $nuevoPersona);
                
        $resultado = serializarArchivo::serializar($listaPersonas);
                
        if($resultado)
            $message = "<p>Dato insertado correctamente</p>";
        else
            $message = "<p>Error al insertar el dato</p>";

        include_once("views/persona/listar.php");
    }

    public static function editar()
    {
        $listaPersonas = serializarArchivo::deserializar();
        $idPersona = $_GET['id'];
            
        for($i = 0; $i < count($listaPersonas); $i++)
        {
            if($listaPersonas[$i]->id == $idPersona)
            {
                $personaActual = $listaPersonas[$i];
                break;
            }
        }    
        
       $eventoFormulario = "actualizar";
       include_once("views/persona/agregarOeditar.php");
    }
        /** Si se pulsó el botón actualizar */
    public static function actualizar()
    {
        $listaPersonas = serializarArchivo::deserializar();
        $actualizarPersona = new Persona((int)$_REQUEST['id'], $_REQUEST['nombre'], $_REQUEST['edad'], $_REQUEST['sexo']);
            
        for($i = 0; $i < count($listaPersonas); $i++)
        {
            if($listaPersonas[$i]->id == $_POST['id'])
            {
                $listaPersonas[$i]->nombre = $_POST['nombre'];
                $listaPersonas[$i]->edad = $_POST['edad'];
                $listaPersonas[$i]->sexo = $_POST['sexo'];
                break;
            }
        }
        
        $resultado = serializarArchivo::serializar($listaPersonas);
        
        if($resultado)
            $message = "<p>Dato actualizado correctamente</p>";
        else
            $message = "<p>Error al actualizar el dato</p>";
        
        include_once("views/persona/listar.php");
    }
        
        
    public static function borrar()
    {
        $listaPersonas = serializarArchivo::deserializar();
        $id = $_GET['id'];
        
        for($i = 0; $i < count($listaPersonas); $i++)
        {        
            if($listaPersonas[$i]->id == $id)
            {
                unset($listaPersonas[$i]);
                $listaPersonas = array_values($listaPersonas);
                break;
            }
        }
        
        $resultado = serializarArchivo::serializar($listaPersonas);
        
        if($resultado)
            $message = "<p>Dato borrado correctamente</p>";
        else
            $message = "<p>Error al borrar el dato</p>";

        include_once("views/persona/listar.php");
    }

    public static function listar()
    {
        $listaPersonas = serializarArchivo::deserializar();
        include_once("views/persona/listar.php");
    }
}
    